var express = require('express');
var router = express.Router();
const userController = require("../controllers/user");
const appointmentController = require("../controllers/appointment");
const auth = require("../helpers/auth");

router.post("/register", userController.register);
router.post("/login", userController.login);
router.get("/appointment", appointmentController.all);
router.post("/appointment", auth.userAuthCheck, appointmentController.create);

module.exports = router;
