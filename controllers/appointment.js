
const appointmentSchema = require("../models/appointment");
const slotSchema = require("../models/slot");
const mongoose = require("mongoose");
const Appointment = mongoose.model("Appointment");
const Slot = mongoose.model("Slot");
const appointmentController = {
    all: function (req, res) {
        // Returns all appointments
        Appointment.find({}).exec((err, appointments) => res.json(appointments));
    },
    create: function (req, res) {
        Slot.findOne({
            slotDate: req.body.slotDate,
            slotInTime: req.body.slotInTime,
            slotOutTime: req.body.slotOutTime
        }, (error, appointment) => {
            if (!error) {
                if (appointment != null) {
                    if (appointment.slotDate === req.body.slotDate && appointment.slotInTime === req.body.slotInTime) {

                        return res.status(400).json({
                            message: 'slot not available',
                        });
                    }

                } else {
                    var newSlot = new Slot({
                        slotDate: req.body.slotDate,
                        slotInTime: req.body.slotInTime,
                        slotOutTime: req.body.slotOutTime,
                        createdAt: Date.now()
                    });
                    newSlot.save();
                    // Creates a new record from a submitted form
                    var newappointment = new Appointment({
                        name: req.body.name,
                        slots: newSlot._id
                    });

                    let msg = req.body.name + " " + "this message is to confirm your appointment for" + " " + req.body.slotDate + " " + "at" + " " + req.body.slotInTime;
                    console.log(msg);

                    newappointment.save((err, saved) => {
                        Appointment.find({ _id: saved._id })
                            .populate("slots")
                            .exec((err, appointment) => res.json(appointment));

                    });
                }

            }
        })
    }
};

module.exports = appointmentController;
