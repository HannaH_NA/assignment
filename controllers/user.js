const userSchema = require("../models/user");
const mongoose = require("mongoose");
const User = mongoose.model("User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const user = {
    //  register function

    register: function (req, res) {
        var data = { username: "", password: "" };
        data = { ...data, ...req.body };
        console.log("hai", data);

        bcrypt.hash(data.password, 10, function (error, hash) {

            if (!error) {
                User.findOne(
                    {
                        username: data.username
                    },
                    function (error, existingUser) {
                        if (!error) {
                            if (existingUser == null) {
                                const myData = new User({
                                    ...data,
                                    password: hash
                                });

                                myData
                                    .save()
                                    .then(response => {
                                        const JWTToken = jwt.sign(
                                            {
                                                _id: response._id,
                                            },
                                            "secretKey",
                                            { expiresIn: "10000d" }
                                        );

                                        console.log("data save to database" + myData);
                                        res.redirect("/login")
                                    })
                            }
                        }
                    }
                );
            } else {
                return res.status(200).json({
                    success: false,
                    error: {
                        types: "Failure",
                        messages: "hashing password failed"
                    }
                });
            }
        });
    },

    //  login function

    login: function (req, res) {
        var data = { username: "", password: "" };
        data = { ...data, ...req.body };
        User.findOne(
            {
                username: data.username
            },
            function (error, user) {
                if (!error) {
                    if (user != null) {
                        bcrypt.compare(data.password, user.password, (err, result) => {
                            console.log("error ", err, "result ", result);
                            if (!err && result) {
                                const JWTToken = jwt.sign(
                                    {
                                        _id: user._id,
                                    },
                                    "secretKey",
                                    { expiresIn: "10000d" }
                                );
                                res.cookie("test", JWTToken, {
                                    maxAge: 900000,
                                    httpOnly: true
                                });

                                return res.status(200).json({
                                    success: true,
                                    data: {
                                        _id: user._id,
                                        username: user.username,
                                        token: JWTToken
                                    }
                                });
                            } else {
                                res.status(200).json({
                                    success: false,
                                    error: {
                                        message: [{ password: "Invalid password" }]
                                    }
                                });
                            }
                        });
                    } else {
                        return res.status(200).json({
                            success: false,
                            error: {
                                types: "Validation Error",
                                messages: [{ username: "user is not registered" }]
                            }
                        });
                    }
                } else {
                    return res.status(200).json({
                        success: false,
                        error: {
                            types: "Failure",
                            messages: "findOne in db failed"
                        }
                    });
                }
            }
        );
    }
};
module.exports = user;
