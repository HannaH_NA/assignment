const appointmentSchema = require("../models/appointment");
const slotSchema = require("../models/slot");
const mongoose = require("mongoose");
const Appointment = mongoose.model("Appointment");
const Slot = mongoose.model("Slot");

const slotController = {
    all: function (req, res) {
        // Returns all Slots
        Slot.find({})
            .exec((err, slots) => res.json(slots))
    },
    create: function (req, res) {

        var newSlot = new Slot({
            slotDate: req.body.slotDate,
            slotInTime: req.body.slotInTime,
            slotOutTime: req.body.slotOutTime,
            createdAt: Date.now()
        });
        newSlot.save((err, saved) => {
            //Returns the new slot
            //after a successful save
            Slot
                .findOne({ _id: saved._id })
                .exec((err, slot) => res.json(slot));
        })
    }
};

module.exports = slotController;