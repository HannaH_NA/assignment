const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema

const appointmentSchema = new Schema({
    name: { type: String, required: true },
    slots: { type: Schema.Types.ObjectId, ref: 'Slot' },
    createdAt: { type: Date, required: false },
});

appointmentSchema.pre("save", function (next) {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    this.updatedAt = now.getTime();
    next();
});
mongoose.model("Appointment", appointmentSchema);