const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema

const slotSchema = new Schema({
    slotDate: { type: String, required: true },
    slotInTime: { type: String, required: true },
    slotOutTime: { type: String, required: true },
    createdAt: { type: Date, required: false },
});

slotSchema.pre("save", function (next) {
    now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    this.updatedAt = now.getTime();
    next();
});
mongoose.model("Slot", slotSchema);
